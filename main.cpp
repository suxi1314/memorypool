#include <iostream>
#include <cassert>
#include <ctime>
#include <vector>

#include "MemoryPool.hpp" // MemoryPool<T>
#include "StackAlloc.hpp" // StackAlloc<T, Alloc>

// number of insert elements
#define ELEMS 1000000
// repeat times
#define REPS 100

int main()
{
    clock_t start;
/*
    // using std::allocator 8.44619s
    StackAlloc<int, std::allocator<int> > stackDefault;
    start = clock();
    for (int j = 0; j < REPS; j++) {
        assert(stackDefault.empty());
        for (int i = 0; i < ELEMS; i++)
            stackDefault.push(i);
        for (int i = 0; i < ELEMS; i++)
            stackDefault.pop();
    }
    std::cout << "Default Allocator Time: ";
    std::cout << (((double)clock() - start) / CLOCKS_PER_SEC) << "\n\n";
    // using std::vector 4.04799 s
    std::vector<int> stackVector;
    start = clock();
    for (int j = 0; j < REPS; j++) {
        for (int i = 0; i < ELEMS; i++)
            stackVector.push_back(i);
        for (int i = 0; i < ELEMS; i++)
            stackVector.pop_back();
    }
    std::cout << "Vector Time: ";
    std::cout << (((double)clock() - start) / CLOCKS_PER_SEC) << "\n\n";
*/
    // using memory pool 3.16696s
    StackAlloc<int, MemoryPool<int>> stackPool;
    start = clock();
    for (int j = 0; j < REPS; j++) {
        for (int i = 0; i < ELEMS; i++)
            stackPool.push(i);
        for (int i = 0; i < ELEMS; i++)
            stackPool.pop();
    }
    std::cout << "MemoryPool Allocator Time: ";
    std::cout << (((double)clock() - start) / CLOCKS_PER_SEC) << "\n\n";
    return 0;
}

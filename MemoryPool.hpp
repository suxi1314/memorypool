#ifndef MEMORY_POOL_HPP
#define MEMORY_POOL_HPP

#include <climits>
#include <cstddef>
// default BlockSize is 4096B
template <typename T, size_t BlockSize = 4096>
class MemoryPool
{
public:
    typedef T*  pointer;
    template <typename U> struct rebind {
        typedef MemoryPool<U> other;
    };
    // default constructor
    // no except: will not throw exception
    MemoryPool() noexcept {
        currentBlock_ = nullptr;
        currentSlot_ = nullptr;
        lastSlot_ = nullptr;
        freeSlots_ = nullptr;
    }
    // destroy the whole memory pool
    ~MemoryPool() noexcept {
        // loop to deallocate blocks
        slot_pointer_ curr = currentBlock_;
        while (curr != nullptr) {
            slot_pointer_ prev = curr->next;
            operator delete(reinterpret_cast<void*>(curr));
            curr = prev;
        }
    }
    // allocate one object one time, ignore n and hint
    pointer allocate(size_t n = 1, const T *hint = 0) {
        if (freeSlots_ != nullptr) {
            pointer result = reinterpret_cast<pointer>(freeSlots_);
            freeSlots_ = freeSlots_->next;
            return result;
        } else { 
            // slot not enough in current block
            if (currentSlot_ >= lastSlot_) {
                // allocate a new block, point to current block
                data_pointer_ newBlock = reinterpret_cast<data_pointer_>(operator new(BlockSize));
                reinterpret_cast<slot_pointer_>(newBlock)->next = currentBlock_;
                currentBlock_ = reinterpret_cast<slot_pointer_>(newBlock);
                // align block for object
                data_pointer_ body = newBlock + sizeof(slot_pointer_);
                uintptr_t result = reinterpret_cast<uintptr_t>(body);
                size_t bodyPadding = (alignof(slot_type_) - result) % alignof(slot_type_);
                currentSlot_ = reinterpret_cast<slot_pointer_>(body + bodyPadding);
                lastSlot_ = reinterpret_cast<slot_pointer_>(newBlock + BlockSize - sizeof(slot_type_));
            }
            return reinterpret_cast<pointer>(currentSlot_++);
        }
    }
    // after destruct of objects, deallocate memory
    void deallocate(pointer p, size_t n = 1) {
        if (p != nullptr) {
           reinterpret_cast<slot_pointer_>(p)->next = freeSlots_;
           freeSlots_ = reinterpret_cast<slot_pointer_>(p);
        }
    }
    // call constructor of new object
    // usning forward transfer parameter to constructor
    template <typename U, typename... Args>
    void construct(U *p, Args&&... args) {
        new (p) U (std::forward<Args>(args)...);
    }
        // deconstruct objects in memory pool
    template <typename U>
    void destroy(U *p) {
        p->~U();
    }
private:
    // object of slot or pointer of slot
    union Slot_ {
        T element;
        Slot_ *next;
    };
    typedef char *data_pointer_;
    typedef Slot_ slot_type_;
    typedef Slot_* slot_pointer_;
    slot_pointer_ currentBlock_; // pointer to current block
    slot_pointer_ currentSlot_; // pointer to current slot
    slot_pointer_ lastSlot_; // pointer to last slot of current block
    slot_pointer_ freeSlots_; // pointer to free slot of current block
    // check if Memory Pool is too small
    static_assert(BlockSize >= 2* sizeof(slot_type_), "BlockSize too small.");
};// end of MemoryPool

#endif // MEMORY_POOL_HPP


    

